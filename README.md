# Sunset server

This is the server software for Sunset.

## Installation

```
git clone https://git.koyu.space/Sunset/server.git sunset-server
cd sunset-server
sudo pip3 install -r requirements.txt
sudo python3 main.py
```