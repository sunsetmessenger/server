from bottle import *
import os.path
import random
import hashlib
import os

m = hashlib.md5()

def sorted_ls(path):
    return os.popen("ls " + path + " -tr").read()

@route("/getverified/<username>")
def getverified(username):
    response.headers['Access-Control-Allow-Origin'] = '*'
    f = open("verified.txt", "r")
    s = f.read()
    f.close()
    isverified = "ERR_UNVERIFIED"
    for x in s.split('\n'):
        if x == username:
            isverified = "OK"
    return isverified

@route("/getop/<username>")
def getop(username):
    response.headers['Access-Control-Allow-Origin'] = '*'
    f = open("ops.txt", "r")
    s = f.read()
    f.close()
    isverified = "ERR_N00B"
    for x in s.split('\n'):
        if x == username:
            isverified = "OK"
    return isverified

@route("/motd")
def motd():
	response.headers['Access-Control-Allow-Origin'] = '*'
	f = open("motd.txt", "r")
	s = f.read()
	f.close()
	return s

@route("/register/<did>/<password>")
def register(did, password):
	directory = "dids/" + did
	if not os.path.exists(directory):
		os.makedirs(directory)
		f = open("dids/" + did + "/password", "w+")
		f.write(password)
		f.close()
		response.headers['Access-Control-Allow-Origin'] = '*'
		return "OK"
	else:
		response.headers['Access-Control-Allow-Origin'] = '*'
		return "ERR_ALREADYEXISTS"

@route("/getauthtoken/<did>/<password>")
def getauthtoken(did, password):
	try:
		f = open("dids/" + did + "/password", "r")
		s = f.read()
		f.close()
		if password == s:
			f = open("dids/" + did + "/authtokens", "a+")
			hash = str(random.getrandbits(128)) + "\n"
			f.write(hash)
			f.close()
			response.headers['Access-Control-Allow-Origin'] = '*'
			return hash
		else:
			return "ERR_AUTHTOKEN"
	except:
		response.headers['Access-Control-Allow-Origin'] = '*'
		return "ERR_AUTHTOKEN"

@route("/gethash")
def gethash():
	hash = str(random.getrandbits(32))
	response.headers['Access-Control-Allow-Origin'] = '*'
	return hash

@route("/setprofile/name/<did>/<authtoken>/<name>")
def setname(did, authtoken, name):
	try:
		f = open("dids/" + did + "/authtokens", "r")
		s = f.read()
		f.close()
		if authtoken in s:
			f = open("dids/" + did + "/name", "w+")
			f.write(name)
			f.close()
			response.headers['Access-Control-Allow-Origin'] = '*'
			return "OK"
		else:
			response.headers['Access-Control-Allow-Origin'] = '*'
			return "ERR_INVALIDAUTHTOKEN"
	except:
		response.headers['Access-Control-Allow-Origin'] = '*'
		return "ERR_SETPROFILE"

@route("/setprofile/picture/<did>/<authtoken>/<picture>")
def setpicture(did, authtoken, picture):
	try:
		f = open("dids/" + did + "/authtokens", "r")
		s = f.read()
		f.close()
		if authtoken in s:
			f = open("dids/" + did + "/picture", "w+")
			f.write(picture)
			f.close()
			response.headers['Access-Control-Allow-Origin'] = '*'
			return "OK"
		else:
			response.headers['Access-Control-Allow-Origin'] = '*'
			return "ERR_INVALIDAUTHTOKEN"
	except:
		response.headers['Access-Control-Allow-Origin'] = '*'
		return "ERR_SETPROFILE"

@route("/getprofile/name/<did>")
def getname(did):
	try:
		f = open("dids/" + did + "/name", "r")
		s = f.read()
		f.close()
		response.headers['Access-Control-Allow-Origin'] = '*'
		return s
	except:
		response.headers['Access-Control-Allow-Origin'] = '*'
		return "ERR_GETPROFILE"

@route("/getprofile/status/<did>")
def getstatus(did):
	try:
		f = open("dids/" + did + "/status", "r")
		s = f.read()
		f.close()
		response.headers['Access-Control-Allow-Origin'] = '*'
		return s
	except:
		response.headers['Access-Control-Allow-Origin'] = '*'
		return "ERR_GETPROFILE"

@route("/getprofile/picture/<did>")
def getppic(did):
	try:
		f = open("dids/" + did + "/picture", "r")
		s = f.read()
		f.close()
		response.headers['Access-Control-Allow-Origin'] = '*'
		return s
	except:
		response.headers['Access-Control-Allow-Origin'] = '*'
		return "ERR_GETPROFILE"

@route("/setprofile/status/<did>/<authtoken>/<status>")
def setstatus(did, authtoken, status):
	try:
		f = open("dids/" + did + "/authtokens", "r")
		s = f.read()
		f.close()
		if authtoken in s:
			f = open("dids/" + did + "/status", "w+")
			f.write(status)
			f.close()
			response.headers['Access-Control-Allow-Origin'] = '*'
			return "OK"
		else:
			response.headers['Access-Control-Allow-Origin'] = '*'
			return "ERR_INVALIDAUTHTOKEN"
	except:
		response.headers['Access-Control-Allow-Origin'] = '*'
		return "ERR_SETPROFILE"

@route("/sendmessage/<did>/<sender>/<to>/<message>/<time>/<authtoken>")
@route("/sendmessage/<did>/<sender>/<to>/<message:re:.+>/<time>/<authtoken>")
def sendmessage(did, sender, to, message, time, authtoken):
	f = open("dids/" + did + "/authtokens", "r")
	s = f.read()
	f.close()
	dids = [d for d in os.listdir('dids/') if os.path.isdir(os.path.join('dids/', d))]
	if to == "freshbot":
		f = open("ops.txt", "r")
		s = f.read()
		f.close()
		isop = False
		for x in s.split('\n'):
			if x == sender:
				isop = True
		if isop:
			f = open("motd.txt", "w")
			f.write("<b>" + message.replace("+", " ") + "</b>")
			f.close()
			return "OK"
		else:
			return "ERR_SENDMESSAGE"
	if to in dids:
		if not sender == to:
			if authtoken in s:
				directory = "messages/" + did + "/" + to
				if not os.path.exists(directory):
 				        os.makedirs(directory)
 				        directory = "messages/" + to + "/" + did
				if not os.path.exists(directory):
					os.makedirs(directory)
				f = open("messages/" + to + "/" + sender + "/chatlog", "a+")
				f.write(time + "#####" + did + "#####" + message + "\n")
				f.close()
				f = open("messages/" + sender + "/" + to + "/chatlog", "a+")
				f.write(time + "#####" + did + "#####" + message + "\n")
				f.close()
				response.headers['Access-Control-Allow-Origin'] = '*'
				return "OK"
			else:
				response.headers['Access-Control-Allow-Origin'] = '*'
				return "ERR_SENDMESSAGE"
		else:
			response.headers['Access-Control-Allow-Origin'] = '*'
			return "ERR_SENDMESSAGE"
	else:
		response.headers['Access-Control-Allow-Origin'] = '*'
		return "ERR_SENDMESSAGE"

@route("/getmessage/<did>/<sender>/<to>/<authtoken>")
def getmessage(did, sender, to, authtoken):
	try:
		f = open("dids/" + did + "/authtokens", "r")
		s = f.read()
		f.close()
		if authtoken in s:
			f = open("messages/" + to + "/" + sender + "/chatlog", "r")
			s = f.read()
			f.close()
			response.headers['Access-Control-Allow-Origin'] = '*'
			return s
		else:
			response.headers['Access-Control-Allow-Origin'] = '*'
			return "ERR_GETMESSAGE"
	except:
		response.headers['Access-Control-Allow-Origin'] = '*'
		return "ERR_GETMESSAGE"

@route("/getmessages/<did>/<authtoken>")
def getmessages(did, authtoken):
	try:
		f = open("dids/" + did + "/authtokens", "r")
		s = f.read()
		f.close()
		if authtoken in s:
			messages = sorted_ls("messages/" + did + "/")
			response.headers['Access-Control-Allow-Origin'] = '*'
			return messages
		else:
			response.headers['Access-Control-Allow-Origin'] = '*'
			return "ERR_GETMESSAGES"
	except:
		response.headers['Access-Control-Allow-Origin'] = '*'
		return "ERR_GETMESSAGES"

@route("/delmessage/<did>/<to>/<msg>/<authtoken>")
def delmessage(did, to, msg, authtoken):
	response.headers['Access-Control-Allow-Origin'] = '*'
	try:
		f = open("dids/" + did + "/authtokens", "r")
		s = f.read()
		f.close()
		if authtoken in s:
			fl = "messages/" + did + "/" + to + "/chatlog"
			f = open(fl, "r")
			lines = f.readlines()
			f.close()
			f = open(fl, "w")
			for line in lines:
				if not msg in line:
					f.write(line)
			f.close()
			fl = "messages/" + to + "/" + did + "/chatlog"
			f = open(fl, "r")
			lines = f.readlines()
			f.close()
			f = open(fl, "w")
			for line in lines:
				if not msg in line:
					f.write(line)
			f.close()
			return "OK"
		else:
			return "ERR_DELMESSAGE"
	except:
			return "ERR_DELMESSAGE"

@route("/")
def index():
	response.headers['Access-Control-Allow-Origin'] = '*'
	return "This server is a <a href=\"https://sunset.koyu.space/\">Sunset</a> server."

run(host="0.0.0.0", port=8700, server="tornado")
